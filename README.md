# PingIP
The repository is created for a challenge to:
1. list the network details in the lan
2. Find network and ping all possible IP's in the network
3. Display the available machines

# Prerequisites
1. Need a Linux environment to execute run.sh file
2. Run the file run.sh with root user
3. Absolute directory is not used to make the script run on any environment

#Supporting documents uploaded
1. Design document uploaded in directory documentation
2. Unit Test documents uploaded in directory test
