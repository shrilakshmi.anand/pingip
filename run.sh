
#!/bash/sh
set -o nounset
set -o pipefail


#Find the NIC of server
#ip -o -4 addr show | awk '{print $1" " $2": "$4}'
ifconfig > temp.txt
echo "-----------------------------------------
     System has the following Network components
------------------------------------------"
cat temp.txt
grep eth temp.txt | awk '{print $1}' > eth.txt
read lan < "eth.txt"
echo "----------------------------------------
 Your Ethernet cards are $lan
----------------------------------------"
grep lo temp.txt | awk '{print $1}' > ln.txt
read lcl < "ln.txt"
echo "Your local network card is $lcl
 ---------------------------------------"

#Finding IP

grep -Po 'addr:\K[^"]+' temp.txt | awk '{print $1}' | head -n 1 > temp.txt
read ip < "temp.txt"
echo "Your IP address is $ip
------------ -----------------------" 

# Ping all servers in the range

echo "$ip"  | grep -oP '(\d{1,3}\.){1,3}' > temp3.txt
read sn < "temp3.txt"
echo "Pinging servers in the Range $sn(1 to 254)..please give a minute or two
-- -------------------------------------------"

# For loop to ping all servers

for i in $(seq 1 254);
do ping -c1 -w1 $sn$i > temp1.txt
#do ping -c1 -w1 $sn$i | grep "1 received"2 > temp1.txt
if grep -q "1 received" temp1.txt;
then echo "Server  $sn$i is available and alive" > active.txt
else echo "Server $sn$i is not available" > inactive.txt
fi
done
echo "--------------------------------------
The active servers are displayed below and also available in the file active.txt and the inactive servers are available in inactive.txt
----------------------------------"
cat active.txt

#Cleanup temp files
rm eth.txt temp.txt temp3.txt ln.txt temp1.txt 
